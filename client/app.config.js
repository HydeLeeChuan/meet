(function(){
    angular
        .module('meetApp')
        .config(calendarConfig);

    calendarConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function calendarConfig($stateProvider,$urlRouterProvider){

        $stateProvider
            // .state('init',{
            //     url:'/',
            //     view:{
            //         "nav":{
            //             templateUrl:"../app/"
            //         }
            //     }
            // })
            .state('login',{
                url : '/',
                views: {
                    "nav": {
                        templateUrl: "./home/navigation.html"
                    },
                    "content": {
                        templateUrl: './users/login.html'
                    }
                },
                controller : 'loginCtrl',
                controllerAs : 'ctrl'
            })
            .state('home',{
                url : '/home/:user',
                views: {
                    "nav": {
                        templateUrl: "./home/navigation.html"
                    },
                    "content": {
                        templateUrl: './calendar/calendar.html'
                    }
                },
                controller : 'calendarCtrl',
                controllerAs : 'ctrl'

            })

            /*.state('calendar',{
                url : '/calendar',
                views: {
                    "nav": {
                        templateUrl: "./home/navigation.html"
                    },
                    "content": {
                        templateUrl: "./calendar/calendar.html"
                    }
                },
                controller : 'calendarCtrl',
                controllerAs : 'ctrl'
            })*/

            .state('meet',{
                url : '/meet',
                params:{event:null},
                views: {
                    "nav": {
                        templateUrl: "./home/navigation.html"
                    },
                    "content": {
                        templateUrl :'./create/create.html'
                    }
            },
            controller : 'createCtrl',
            controllerAs : 'ctrl'

        })
            .state('meet1',{
                url : '/meet1/:username',
                templateUrl :'./create/create.html',
                // params: {start:user.start,
                //     end:user.end},
                controller : 'createCtrl',
                controllerAs : 'ctrl'

        })
            .state('back',{
                url : '/back',
                templateUrl :'./home/home.html',
                controller : 'calendarCtrl',
                controllerAs : 'ctrl'
            })
            // .state('map',{
            //     url: '/map',
            //     templateUrl:'./map/map.html',
            //     controller: 'mapCtrl',
            //     controllerAs: 'ctrl'
            //
            // });
        .state("SignUp", {
            url: "/signUp",
            views: {
                "nav": {
                    templateUrl: "./home/navigation.html"
                },
                "content": {
                    templateUrl: "./users/register.html"
                }
            },
            controller : 'RegisterCtrl',
            controllerAs : 'ctrl'
        });


        $urlRouterProvider.otherwise("/");
    }



})();