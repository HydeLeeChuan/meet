(function(){
    angular
        .module('meetApp')
        .service("eventService",eventService);

    eventService.$inject = ["$http","$q"];

    function eventService($http,$q){
        var vm = this;
        vm.eventInit = _eventInit;
        vm.eventFriend = _eventFriend;
        vm.eventCreate = _eventCreate;
        vm.eventUpdate = _eventUpdate;
        vm.eventSearch = _eventSearch;

        function _eventInit(req){
            var defer = $q.defer();
            console.log(JSON.stringify(req.body))

            $http
                .get("/api/events",{
                    params:{
                        userID: req
                    }

                })
                .then(function(res){
                    delete res.data['_id'];
                    delete res.data['__v'];

                    defer.resolve(res);

                })
                .catch(function(err){
                    console.log(JSON.stringify(err));
                });
            return(defer.promise);
        }

        function _eventSearch(req){
            var defer = $q.defer();
            console.log(JSON.stringify(req.body));

            $http
                .get("/api/events/event",{
                    params:{
                        eventID: req
                    }

                })
                .then(function(res){
                    delete res.data['_id'];
                    delete res.data['__v'];

                    defer.resolve(res);

                })
                .catch(function(err){
                    console.log(JSON.stringify(err));
                });
            return(defer.promise);
        }

        function _eventFriend(event){
            var defer = $q.defer();
            console.log("in event Friend" + JSON.stringify(event));

            $http
                .get("/api/events/friend",{
                    params: {
                        userID: event
                    }
                })
                .then(function(res){
                    delete res.data['_id'];
                    delete res.data['__v'];

                    defer.resolve(res);

                })
                .catch(function(err){
                    console.log(JSON.stringify(err));
                });
            return(defer.promise);
        }

        function _eventCreate(event){
            var defer = $q.defer();
            $http
                .post("/api/events",event)
                .then(function(res){
                    console.log(res);
                    delete res.data['_id'];
                    delete res.data['__v'];

                    defer.resolve(res);


                })
                .catch(function(err){
                    // console.log(JSON.stringify(err));
                });
            return(defer.promise);


        }

        function _eventUpdate(event) {
            console.log("_event update" + JSON.stringify(event));
            var defer = $q.defer();
            $http.put("/api/events", event)
                .then(function (result) {
                    console.log("ok");
                    defer.resolve(result);
                })
                .catch(function (err) {
                    console.log("shit");
                    defer.reject(err);
                });
            return defer.promise;
        }

        function _eventUpdate(event) {
            console.log("_event update" + JSON.stringify(event));
            var defer = $q.defer();
            $http.put("/api/events/A", event)
                .then(function (result) {
                    console.log("ok");
                    defer.resolve(result);
                })
                .catch(function (err) {
                    console.log("shit");
                    defer.reject(err);
                });
            return defer.promise;
        }



    }

})();