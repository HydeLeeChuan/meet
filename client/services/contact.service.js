(function(){
    angular
        .module('meetApp')
        .service("contactService",contactService);

    contactService.$inject = ["$http","$q"];

    function contactService($http,$q){
        var service = this;
        service.userInit = _userInit;


        function _userInit(){

            var defer = $q.defer();
            $http
                .get("/api/users",{

                })
                .then(function(res){
                    delete res.data['_id'];
                    delete res.data['__v'];
                    console.log(JSON.stringify(res.data));
                    defer.resolve(res);

                })
                .catch(function(err){
                    console.log(JSON.stringify(err));
                });
            return(defer.promise);
        }


    }

})();