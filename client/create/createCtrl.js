(function(){
    angular
        .module("meetApp")
        .controller("createCtrl",createCtrl);

    createCtrl.$inject = ["$http","$q","eventService", "$stateParams", "$state", "$scope"];

    function createCtrl($http,$q,eventService, $stateParams, $state, $scope){
        var vm = this;
        vm.result = null;

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        vm.event = {};





        // $scope.eventSources = [ $scope.events ];




            if($stateParams.event){
                console.log("createCtrl + " + JSON.stringify($stateParams.event));
                if($stateParams.event.pending == true){
                    eventService.eventSearch($stateParams.event.eventID)
                    .then(function(res){
                        console.log("displaying" + JSON.stringify(res.data));
                        vm.event = res.data;
                    })
                        .catch(function(err){

                        });

                }else{
                    vm.event = $stateParams.event;
                }
            }

        vm.approve = function(){
            var editEvent = {
                eventID: vm.event.eventID,
                approval:true
            };
            eventService.eventUpdate(editEvent)
                .then(function(res){
                    console.log(res);
                })
                .catch(function(){

                });
            $state.go("home",{user:vm.event.userID});
        };


        vm.createEvent = function(){
            console.log("in create Event");
            var pEvent = {
                eventID:vm.event.eventID,
                userID: vm.event.userID,
                friendID: vm.event.friendID,
                title:vm.event.title,
                start:vm.event.start,
                end:vm.event.end,
                allDay: vm.event.allDay,
                approval:false
            };
            console.log("event made!>>>" + JSON.stringify(pEvent));
            eventService.eventCreate(pEvent)
                .then(function(res){
                    // console.log("event made!>>>" + vm.event.username);
                    $state.go("home",{user:vm.event.userID});


                })
                .catch(function(err){

                })
        };
    }

})();