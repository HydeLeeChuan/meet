(function () {
    angular
        .module('meetApp')
        .controller('calendarCtrl',calendarCtrl);

    calendarCtrl.$inject=[ "$http","$scope", "$compile","uiCalendarConfig","eventService","$q","contactService","$state","$uibModal","$log", "$document", "$stateParams"];

    function calendarCtrl($http, $scope, $compile, uiCalendarConfig, eventService, $q, userService, $state, $uibModal, $log, $document, $stateParams) {

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        var vm  = this;


        vm.user ={};
        vm.myCalendar = [];
        vm.events = [];
        $scope.events=[];
        $scope.eventSources = [ $scope.events ];
        vm.pEvent ={};

        // eventService.event()





        $scope.changeView = function (view, calendar) {
            uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
        };
        /* Change View */
        $scope.renderCalender = function (calendar) {
            if (uiCalendarConfig.calendars[calendar]) {
                uiCalendarConfig.calendars[calendar].fullCalendar('render');
            }
        };
        /* Render Tooltip */
        $scope.eventRender = function (event, element, view) {
            element.attr({
                'tooltip': event.title,
                'tooltip-append-to-body': true
            });
            $compile(element)($scope);
        };




        $scope.uiConfig = {
            calendar: {
                height: 450,
                editable: true,
                selectable: true,
                selectHelper: true,

                select: function(start, end, jsEvent) {
                    var eventData = {
                        userID: vm.user,
                        friendID:vm.auser,
                        title: '',
                        start: start,
                        end: end
                    };
                    $state.go("meet", {event:eventData});

                    // eventService.eventCreate(eventData)
                    // .then(function(res){
                    //     console.log("at select" + JSON.stringify(res.data));
                    //     var eventID = res.data.eventID;
                    //     $state.go("meet", {eventID:eventID});
                    // })
                    // .catch(function(){
                    //
                    // });





                    // var title = prompt('Event Title:');


                    // var eventData;
                    // if (title) {
                    //     eventData = {
                    //         userID: vm.user,
                    //         friendID:vm.auser,
                    //         title: title,
                    //         start: start,
                    //         end: end
                    //     };
                    //     console.log(JSON.stringify(eventData));
                    //     eventService.eventCreate(eventData);
                    //     $('#calendar').fullCalendar('renderEvent', eventData, true);
                    // }
                    // $('#calendar').fullCalendar('unselect');
                },
                eventResize: function(event, delta, revertFunc, jsEvent, ui, view){
                    alert(event.title + " end is now " + event.end.format() );
                    console.log(JSON.stringify(event.end));
                    var editEvent = {
                        eventID:event.eventID,
                        end:event.end
                    };

                    if (!confirm("is this okay?")) {
                        revertFunc();
                    }
                    eventService.eventUpdate(editEvent)
                        .then(function(res){
                            console.log(res);
                        })
                        .catch(function(){

                        });

                }

            },

                header: {
                    left: '',
                    center: 'title',
                    right: 'today prev,next'

                },
                eventClick: $scope.alertEventOnClick,
                eventDrop: $scope.alertOnDrop,
                eventResize: $scope.alertOnResize,
                dayClick: $scope.alertEventOnClickday,
                eventRender: $scope.eventRender
        };



        $scope.itemClicked = function ($index) {
            $scope.selectedIndex = $index;
        };

        vm.meet = function(user){
            console.log("index value: "+ JSON.stringify(user));
            vm.auser =user;
            eventService.eventFriend(user)
                .then(function (res) {
                    $scope.calEventsExt = {
                        color: '#f00',
                        textColor: 'yellow',
                        events:res.data};
                    $('#calendar').fullCalendar('addEventSource', $scope.calEventsExt);
                    // $scope.eventSources = [$scope.events];
                    //todo:put in view change to the current week


                })
                .catch(function (err) {
                    console.log(err);

                });


        };

        vm.searchEvent = function(search){
            var search2 = {
                pending:true,
                eventID: search
            };
            $state.go("meet", {event:search2});
        };

        $scope.remove = function (index) {
            $scope.events.splice(index, 1);
        };

        if($stateParams.user){

            console.log("at state params " + JSON.stringify($stateParams));
            vm.user=$stateParams.user;
            console.log("the user id here:" + JSON.stringify(vm.user));

            eventService.eventInit(vm.user)
                .then(function (res) {
                    vm.events = res.data;
                    vm.newEvents = vm.events.filter(function(event){
                        return event.approval !== false;
                    });
                    $('#calendar').fullCalendar('addEventSource', vm.newEvents);
                    console.log(JSON.stringify(vm.events));
                    var rEvent = vm.events.filter(function(event){
                        return event.approval == false;
                    });
                    vm.pEvents = [];
                    function add(event){
                    vm.pEvents = event
                    }
                    add(rEvent);

                    // var event = $.grep(vm.events, function(e){
                    //     return e.approval == false;
                    // });
                    // function add(event){
                    //     vm.pEvent.push(event);
                    // }
                })
                .catch(function (err) {
                    console.log(err);

                });



            userService.userInit(vm.user)
                .then(function (res) {
                    console.log("in calendarCtrl userInit" + JSON.stringify(res.data));
                    vm.users = res.data;
                    var userID = res.data._id;
                    console.log("in calendarCtrl userInit" + JSON.stringify(userID));
                })
                .catch(function (err) {
                    console.log(err);

                });

        }
    }
})();