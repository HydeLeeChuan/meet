(function () {
    angular
        .module("meetApp")
        .controller("HomeCtrl", ["$q", "AuthFactory", HomeCtrl]);

    function HomeCtrl($q, AuthFactory){
        var vm = this;
        AuthFactory.getUserStatus(function(result){
            console.log(JSON.stringify(result));
            vm.isUserLogon = result;
        });
        vm.isUserLogon = AuthFactory.isLoggedIn();

        var defer = $q.defer();
        vm.err = null;
    }

})();