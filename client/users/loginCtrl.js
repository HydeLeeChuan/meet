(function(){
    angular
        .module("meetApp")
        .controller("loginCtrl", loginCtrl);

    loginCtrl.$inject = ["$state", "AuthFactory","Flash"];
    function loginCtrl ($state, AuthFactory, Flash) {
        var vm = this;

        vm.user = {
            username: "LeeChuan",
            password: "LeeChuan"
        };
        vm.login = function (){
            AuthFactory.login(vm.user)
                .then(function(res) {
                    console.log("redirecting");
                    if(AuthFactory.isLoggedIn()){
                    vm.username = "";
                    vm.password = "";
                        console.log(res);
                    $state.go("home",{user:res});
                }else{
                    Flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
                    $state.go("login");
                }
        }).catch(function(){
            console.error("Error Logging on!");
        });
        }

    }

})();