/**
 * Created by phangty on 10/11/16.
 */
(function () {
    angular
        .module("meetApp")
        .controller("RegisterCtrl", ["$sanitize", "$state", "AuthFactory", "Flash", RegisterCtrl]);

    function RegisterCtrl($sanitize, $state, AuthFactory, Flash){
        var vm = this;
        vm.username = "";
        vm.password = "";
        vm.confirmpassword = "";

        vm.register = function () {
            AuthFactory.register($sanitize(vm.username), $sanitize(vm.password))
                .then(function () {
                    vm.disabled = false;
                    vm.username = "";
                    vm.password = "";
                    vm.confirmpassword = "";
                    Flash.clear();
                    Flash.create('success', "Successfully sign up with us, Please proceed to login", 0, {class: 'custom-class', id: 'custom-id'}, true);
                    $state.go("login");
                }).catch(function () {
                console.error("registration having issues");
            });
        };

    }
})();