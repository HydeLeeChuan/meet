(function(){
    angular
        .module('meetApp')
        .controller('logoutCtrl',logoutCtrl);
    logoutCtrl.$inject = ["$state","AuthFactory"];

    function logoutCtrl($state,AuthFactory){
        var vm = this;

        vm.logout = function (){
            AuthFactory.logout()
                .then(function() {
                    $state.go('SignIn');
                }).catch(function(){
                    console.error("Error logging on!");
                });
        };
    }


})();