var express = require("express");
var bodyParser =require("body-parser");
var cookieParser = require("cookie-parser");
var session = require('express-session');
var passport = require('passport');


var config = require("./config");

var app = express();




app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(express.static(__dirname + "/../client"));

app.use(session({
    secret: "meetApp-secret",
    resave: false,
    saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

require('./auth.js')(app, passport);
require("./routes")(app, passport);

// var OpenGraph = require('facebook-open-graph');
// var openGraph = new OpenGraph('MEET');


// routes.init(app, passport);
// routes.create(app, passport);

app.listen(config.port,function(){
    console.info("Webserver started on" + config.port);
});

// enable fb:explicitly_shared
// var options = true;
//
// ... or pass your own custom headers
// options = {
//     'fb:explicitly_shared': true
// };