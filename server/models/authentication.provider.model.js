var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = function(){
    var authentication_providerSchema = new Schema({
        id: {
            type: Number,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
        },
        providerId: {
            type: String,
                allowNull: true
        },
        userId: {
            type: Number,
                allowNull: false
        },
        providerType: {
            type: String,
                allowNull: true
        },
        displayName: {
            type: String,
                allowNull: true
        }
        // ,
        // profile_photo: {
        //     type: 'BLOB',
        //         allowNull: true
        // }

    },{
        timestamps:{
            createAt: 'created_at',
            updatedAt:'updated_at'
        }
    });
     return mongoose.model('authentication_provider', authentication_providerSchema);
};