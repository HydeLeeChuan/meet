var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = function(){
    var EventSchema = new Schema({
        userID: String,
        friendID: String,
        eventID: String,
        title: String,
        allDay:Boolean,
        start: Date,
        end: Date,
        approval: Boolean,
        createdBy: String
    },{
        timestamps:{
            createAt: 'created_at',
            updatedAt:'updated_at'
        }
    });
    mongoose.model('Event', EventSchema);
};