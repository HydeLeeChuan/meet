var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = function(){
    var UserSchema = new Schema({
        username: String,
        email: String,
        password: String
    });
    mongoose.model('User', UserSchema);
};