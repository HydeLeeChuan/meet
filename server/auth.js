var LocalStrategy = require("passport-local").Strategy;
// var GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
// var FacebookStrategy = require("passport-facebook").Strategy;
var bcrypt = require('bcryptjs');

var User = require("./database").User;
var AuthProvider = require("./database").AuthProvider;
var config = require("./config");

module.exports = function (app, passport) {
    function authenticate(username, password, done) {
        // User.find(function(err, docs){
        //     if (err) return next(err);
        //     res.send(docs);

        // });

        console.log("at authenticate " + password);

        User
            .findOne(
                {"username": username},
                function (err, user) {
                    // check if there's a db error
                    // if there is return err
                    if (err)
                        return done(err, false);

                    // check if a user is found
                    // if not found return null
                    if (!user) {
                        return done(null, false);

                    }
                    // else compare password and if match return user, otherwise return null
                    else {
                        if (bcrypt.compareSync(password, user.password)) {
                            console.log("at authenticate 2" + password);
                            return done(null, user);
                        } else {
                            return done(null, false);
                        }
                    }
                });
    }

    //     .then(function(res){
    //     console.log("found");
    //     if(!res){
    //         return done(null, false);
    //     }else{
    //         if(bcrypt.compareSync(password, res.password)){
    //             return done(null, res);
    //         }else{
    //             return done(null, false);
    //         }
    //     }
    // }).catch(function(err){
    //     return done(err, false);
    // });


    // function verifyCallback(accessToken, refreshToken, profile, done){
    //     if(profile.provider === 'google' || profile.provider === 'facebook' ){
    //         id = profile.id;
    //         email = profile.emails[0].value;
    //         displayName = profile.displayName;
    //         provider_type = profile.provider;
    //         var hashpassword = bcrypt.hashSync('socialpwd', bcrypt.genSaltSync(8), null);
    //         User.findOrCreate(
    //             {where:{email:email},
    //                 defaults: {username:email,
    //                     email: email,
    //                     password: hashpassword,
    //                     name:displayName
    //                 }
    //             })
    //             .spread(function(user, created){
    //                 console.log(user.get({
    //                     plain: true
    //                 }));
    //                 console.log(created);
    //                 AuthProvider.findOrCreate({where:{
    //                     userid: user.id,
    //                     providerType: provider_type
    //                 },
    //                 defaults: {
    //                     providerId: id,
    //                     userId: user.id,
    //                     providerType: provider_type,
    //                     displayName: displayName
    //                 }})
    //                     .spread(function(provider,created){
    //                        console.log(provider.get({
    //
    //                             plain:true
    //                         }));
    //                 console.log(created);
    //
    //             });
    //                 done(null, user);
    //     });
    //     }else{
    //         done(null,false);
    //     }
    // }
    // passport.use( new GoogleStrategy({
    //     clientID: config.GooglePlus_key,
    //     clientSecret: config.GooglePlus_secret,
    //     callbackURL: config.GooglePlus_callback_url
    // }, verifyCallback));
    //
    // passport.use(new FacebookStrategy({
    //     clientID: config.Facebook_key,
    //     clientSecret: config.Facebook_secret,
    //     callbackURL: config.Facebook_callback_url,
    //     profileFields: ['id','displayName','photos', 'email']
    // }, verifyCallback));

    passport.use(new LocalStrategy({
        usernameField: "username",
        passwordField: "password"
    }, authenticate));

    passport.serializeUser(function (user, done) {
        console.info("serial to session");
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        console.info("deserial to session");
        console.info("deserial to session " + user.username);

        User.findOne(
            { username: user.username } , function (err, result) {
            if (result) {
                console.info("deserial to session 2: " + user.username);
                done(null, result);
            }
        });
    })
};