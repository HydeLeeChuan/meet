var config = require("./config");
var mongoose = require('mongoose');


mongoose.Promise = global.Promise;


mongoose.connect('mongodb://localhost/meetDB', { config: { autoIndex: false }});
// mongoose.connect('mongodb://admin:admin1q2w@ds155087.mlab.com:55087/mlab');
// mongodb://<dbuser>:<dbpassword>@ds155087.mlab.com:55087/mlab

var db = mongoose.connection;
db.on('error',console.error.bind(console, 'connection error:'));
db.once('open', function(){
    console.log('DB connected!');

});

require('./models/event.model.js')();
require('./models/user.model.js')();
require("./models/authentication.provider.model.js")();


var Event = mongoose.model('Event');
var User = mongoose.model('User');


var AuthProvider = mongoose.model('authentication_provider');



module.exports = {
    User: User,
    Event: Event,
    AuthProvider: AuthProvider
};

