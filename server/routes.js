var EventController = require('./api/event/event.controller');
var UserController = require('./api/user/user.controller');

// var fs = require('fs');
// const Console = require('console');

var express = require("express");
var config = require("./config");

const API_EVENTS_URI = "/api/events";
const API_USERS_URI = "/api/users";

module.exports = function (app, passport){
//initialising account
    app.get(API_EVENTS_URI, isAuthenticated, EventController.list);
    app.get("/api/events/friend", isAuthenticated, EventController.listfriend);
    app.get(API_USERS_URI, isAuthenticated, UserController.list);
    app.get("/api/events/event", isAuthenticated, EventController.listevent);


//account activities
    app.post(API_EVENTS_URI, isAuthenticated, EventController.create);
    app.put(API_EVENTS_URI, isAuthenticated, EventController.update);
    app.put("/api/events/A", isAuthenticated, EventController.updateA);

// function errorHandler(app){
//     app.use(function(req, res){
//         res.status(401).sendFile(CLIENT_FOLDER + "/app/errors/404.html");
//     });
//     app.use(function (err, req, res, next) {
//         res.status(500).sendFile(path.join(CLIENT_FOLDER + '/app/errors/500.html'));
//     });
//
//
// }
    app.post('/register', UserController.register);

    app.get('/calendar', isAuthenticated, function(req, res){
        res.redirect('../');
    });

    app.post("/login", passport.authenticate("local",{
        successRedirect:"/#/home",
        failureRedirect:"/#/login",
        failureFlash : true

    }));

    app.get("/oauth/google",passport.authenticate("google",{
        scope: ["email", "profile" ]
    }));

    app.get("/oauth/google/callback", passport.authenticate("google",{
        successRedirect:"/calendar",
        failureRedirect:"/signUp"
    }));

    app.get("/oauth/facebook", passport.authenticate("facebook",{
        scope: ["email","public_profile"]
    }));

    app.get("/oauth/facebook/callback", passport.authenticate("facebook",{
        successRedirect:"/calendar",
        failureRedirect:"/signUp"
    }));

    app.get("/status/user", function(req, res){
        var status1 = "";
        var userID1 = "";
        if(req.user){
            status1 = req.user.username;
            userID1 = req.user._id;
        }
        // const output = fs.createWriteStream('./stdout.log');
        // const errorOutput = fs.createWriteStream('./stderr.log');
        // const logger = new Console(output, errorOutput);
        //
        //
        // logger.log(JSON.stringify(req.user));
        console.log("app get user info: xyzppppppppppp " + JSON.stringify());
        // console.info("status of the user --> " + status);
        // res.send(userID);
        res.send({status: status1, userID: userID1 }).end();
        // res.send(userID);


    });

    app.get("/logout", function(req,res){
        req.logout();
        req.session.destroy();
        res.send(req.user).end();
    });




    // init: initRouteHandler,
    // create: createRouteHandler,
    // errorHandler: errorHandler

    function isAuthenticated(req, res, next) {
        // console.log(req);
        if (req.isAuthenticated())
            return next();
        res.redirect('/');
    }


};