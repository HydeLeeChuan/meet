var Event = require("../../database").Event;

exports.list = function(req, res, next) {
    console.log('requesting for events '+ JSON.stringify(req.query));
    Event
        .find({
                'userID':req.query.userID
            },'eventID title allDay start end approval',
            function(err, docs){
                if (err) return next(err);
                console.log("manage to send docs " + JSON.stringify(docs));
                res.send(docs);
            }
        )
};

exports.listfriend = function(req, res, next) {
    console.log('requesting for events '+ JSON.stringify(req.query));
    Event
        .find({
                'userID':req.query.userID
            },'id allDay start end',
            function(err, docs){
                if (err) return next(err);
                console.log("manage to send docs " + JSON.stringify(docs));
                res.send(docs);
            }
        )
};

exports.listevent = function(req, res, next) {
    console.log('requesting for events list event'+ JSON.stringify(req.query));
    Event
        .findOne({
                'eventID':req.query.eventID
            },'userID friendID title allDay eventID allDay start end',
            function(err, docs){
                if (err) return next(err);
                console.log("manage to send docs " + JSON.stringify(docs));
                res.send(docs);
            }
        )
};



exports.create = function(req,res, next){
    console.log('inserting into event:' + JSON.stringify(req.body));
    Event.create(req.body, function(err, doc) {
        if (err) return next(err);
        // console.log(JSON.stringify(doc._id));
        doc.eventID = doc._id;
        doc.save(function (err, docx) {
            if (err) return handleError(err);
            // delete docx[_id];
            //console.log(JSON.stringify(req.body.friendID));
            if(req.body.friendID != ""){
                var event2 = {
                    userID: req.body.friendID,
                    friendID: req.body.userID,
                    title: req.body.title,
                    start: req.body.start,
                    end: req.body.end,
                    allDay: req.body.allDay,
                    approval:false
                };
                exports.create2(event2);
            }
            doc.eventID = doc._id;


            console.log(JSON.stringify(docx));
            res.send(docx);
        });

    });

};

exports.create2 = function(req,res, next){
    console.log('inserting into event:' + JSON.stringify(req));
    Event.create(req, function(err, doc) {
        if (err) return next(err);
        // console.log(JSON.stringify(doc._id));
        doc.eventID = doc._id;
        doc.save(function (err, docx) {
            if (err) return handleError(err);
            // delete docx[_id];
            //console.log(JSON.stringify(req.body.friendID));

            console.log(JSON.stringify(docx));
        });

    });

};



exports.update = function(req,res, next){
    // console.log("HELLLO" + JSON.stringify(req.body));

    Event
        .findByIdAndUpdate({
                _id: req.body.eventID

        },{$set:{end:req.body.end}},{new:true}, function(err, doc) {
            if (err) return next(err);
            console.log(JSON.stringify(doc));
                res.send(doc);
        });
};

exports.updateA = function(req,res, next){
    console.log("seeking approval" + JSON.stringify(req.body));

    Event
        .findByIdAndUpdate({
            _id: req.body.eventID

        },{$set:{approval:req.body.approval}},{new:true}, function(err, doc) {
            if (err) return next(err);
            console.log(JSON.stringify(doc));
            res.send(doc);
        });
};
